import shutil
import os.path

import matsim.runtime.eqasim as eqasim

def configure(context):
    context.stage("matsim.runtime.java")
    context.stage("matsim.runtime.eqasim")
        
    context.stage("matsim.scenario.population")
    context.stage("matsim.scenario.households")
    context.stage("matsim.scenario.facilities")
    context.stage("matsim.scenario.network.mapped")
        
    context.config("input_downsampling")
    context.config("threads")
    context.config("random_seed")
    
    context.config("output_prefix", "switzerland_")


def execute(context):
    # Some files we just copy
    transit_vehicles_input_path = context.stage("matsim.scenario.network.mapped")["vehicles"]
    transit_vehicles_output_path = "%s/%stransit_vehicles.xml.gz" % (context.path(), context.config("output_prefix"))
    shutil.copyfile(transit_vehicles_input_path, transit_vehicles_output_path)

    households_input_path = context.stage("matsim.scenario.households")
    households_output_path = "%s/%shouseholds.xml.gz" % (context.path(), context.config("output_prefix"))
    shutil.copyfile(households_input_path, households_output_path)
        
    # Some files we send through several preparation scripts
    
    # Run preparation
    facilities_input_path = context.stage("matsim.scenario.facilities")
    facilities_output_path = "%sfacilities.xml.gz" % context.config("output_prefix")
    
    population_input_path = context.stage("matsim.scenario.population")
    population_prepared_path = "prepared_population.xml.gz"
    
    network_input_path = context.stage("matsim.scenario.network.mapped")["network"]
    network_output_path = "%snetwork.xml.gz" % context.config("output_prefix")

    # Call the basic preparation script
    eqasim.run(context, "org.eqasim.core.scenario.preparation.RunPreparation", [
        "--input-facilities-path", facilities_input_path,
        "--output-facilities-path", facilities_output_path,
        "--input-population-path", population_input_path,
        "--output-population-path", population_prepared_path,
        "--input-network-path", network_input_path,
        "--output-network-path", network_output_path,
        "--threads", context.config("threads")
    ])
    
    assert os.path.exists("%s/%sfacilities.xml.gz" % (context.path(), context.config("output_prefix")))
    assert os.path.exists("%s/prepared_population.xml.gz" % context.path())
    assert os.path.exists("%s/%snetwork.xml.gz" % (context.path(), context.config("output_prefix")))

    # Generate the config file
    config_path = "%sconfig.xml" % context.config("output_prefix")
    
    eqasim.run(context, "org.eqasim.core.scenario.config.RunGenerateConfig", [
        "--output-path", config_path,
        "--prefix", context.config("output_prefix"),
        "--sample-size", context.config("input_downsampling"),
        "--random-seed", context.config("random_seed"),
        "--threads", context.config("threads")
    ])
    
    assert os.path.exists("%s/%sconfig.xml" % (context.path(), context.config("output_prefix")))
    
    # Calculate the stop categories
    transit_schedule_input_path = context.stage("matsim.scenario.network.mapped")["schedule"]
    transit_schedule_output_path = "%stransit_schedule.xml.gz" % context.config("output_prefix")

    eqasim.run(context, "org.eqasim.switzerland.scenario.RunCalculateStopCategories", [
        "--input-path", transit_schedule_input_path,
        "--output-path", transit_schedule_output_path
    ])
    
    assert os.path.exists("%s/%stransit_schedule.xml.gz" % (context.path(), context.config("output_prefix")))
    
    # Route the population
    population_output_path = "%spopulation.xml.gz" % context.config("output_prefix")
    
    eqasim.run(context, "org.eqasim.core.scenario.routing.RunPopulationRouting", [
        "--config-path", config_path,
        "--output-path", population_output_path,
        "--threads", context.config("threads"),
        "--config:plans.inputPlansFile", population_prepared_path
    ])
    
    assert os.path.exists("%s/%spopulation.xml.gz" % (context.path(), context.config("output_prefix")))

    # Validate the scenario
    eqasim.run(context, "org.eqasim.core.scenario.validation.RunScenarioValidator", [
        "--config-path", config_path
    ])

    # Adapt the config
    eqasim.run(context, "org.eqasim.switzerland.scenario.RunAdaptConfig", [
        "--input-path", config_path,
        "--output-path", config_path,
        "--downsamplingRate", context.config("input_downsampling"),
        "--replanningRate", "0.05",
        "--hasFreight", context.config("use_freight")
    ])
    
    assert os.path.exists("%s/%sconfig.xml" % (context.path(), context.config("output_prefix")))
    
    # Cleanup
    os.remove("%s/prepared_population.xml.gz" % context.path())

    return "%sconfig.xml" % context.config("output_prefix")
